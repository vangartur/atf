package utils;

import java.util.Map;

public class ScenarioContext {
    Map<DataKeys, Object> data;

    public void saveData(DataKeys key, Object object) {
        data.put(key, object);
    }

    public Object getData(DataKeys key) {
        return data.get(key);
    }

    public void clearData() {
        data.clear();
    }

}
