package stepdefinitions;

import io.cucumber.java.en.Given;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.safari.SafariDriver;

public class StepDefinitions {

    @Given("some given is here")
    public void someGivenIsHere() {
        WebDriver driver = new SafariDriver();

        driver.get("https://www.google.md");
    }
}
